"""
Defines unittest for the non database functions of the db lib.
"""


from __future__ import unicode_literals
import unittest
from sitelib.db.models import Site
from sitelib.db import lib


class IndependentTestCase(unittest.TestCase):
    """
    @summary: Defines the test cases that do not need the database connection.
    """
    
    
    def test_get_by_internal_id_from_iterable(self):
        """
        @summary: Test getting a Site from a iterable with the specified
        internal id.
        """
        
        testSite = Site()
        testSite.internal_id = "internal"
        testSite.public_id = "public"
        
        sites = set((testSite,))
        
        found_site = lib.get_by_internal_id_from_iterable(sites=sites,
                                                      internal_id="internal",
                                                      deleted=None,
                                                      disabled=None)
        
        self.assertIsInstance(found_site, Site)
        self.assertEqual(found_site, testSite)
        
        found_site = lib.get_by_internal_id_from_iterable(sites=sites,
                                                      internal_id="internal2",
                                                      deleted=None,
                                                      disabled=None)
        
        self.assertIsNone(found_site)
        
        testSite.deleted = True
        
        found_site = lib.get_by_internal_id_from_iterable(sites=sites,
                                                      internal_id="internal",
                                                      deleted=False,
                                                      disabled=None)
        
        self.assertIsNone(found_site)
        
        found_site = lib.get_by_internal_id_from_iterable(sites=sites,
                                                      internal_id="internal",
                                                      deleted=True,
                                                      disabled=None)
        
        self.assertEqual(found_site, testSite)
        
        testSite.deleted = False
        testSite.disabled = True
        
        found_site = lib.get_by_internal_id_from_iterable(sites=sites,
                                                      internal_id="internal",
                                                      deleted=None,
                                                      disabled=False)
        
        self.assertIsNone(found_site)
        
        found_site = lib.get_by_internal_id_from_iterable(sites=sites,
                                                      internal_id="internal",
                                                      deleted=None,
                                                      disabled=True)
        
        self.assertEqual(found_site, testSite)
    
    def test_get_by_internal_ids_from_iterable(self):
        """
        @summary: Test getting a Site from a iterable with the specified
        internal ids.
        """
        
        testSite = Site()
        testSite.internal_id = "internal"
        testSite.public_id = "public"
        
        testSite2 = Site()
        testSite2.internal_id = "internal2"
        testSite2.public_id = "public2"
        
        sites = set((testSite, testSite2))
        
        found_sites = lib.get_by_internal_ids_from_iterable(sites=sites,
                                      internal_ids=["internal", "internal2"],
                                      deleted=None,
                                      disabled=None)
        
        self.assertIsInstance(found_sites, dict)
        self.assertEqual(found_sites["internal"], testSite)
        self.assertEqual(found_sites["internal2"], testSite2)
        
        found_sites = lib.get_by_internal_ids_from_iterable(sites=sites,
                                      internal_ids=["internal3", "internal4"],
                                      deleted=None,
                                      disabled=None)
        
        self.assertIsInstance(found_sites, dict)
        self.assertIsNone(found_sites["internal3"])
        self.assertIsNone(found_sites["internal4"])
        
        testSite.deleted = True
        found_sites = lib.get_by_internal_ids_from_iterable(sites=sites,
                                      internal_ids=["internal", "internal2"],
                                      deleted=False,
                                      disabled=None)
        
        self.assertIsInstance(found_sites, dict)
        self.assertIsNone(found_sites["internal"])
        self.assertEqual(found_sites["internal2"], testSite2)
        
        found_sites = lib.get_by_internal_ids_from_iterable(sites=sites,
                                      internal_ids=["internal", "internal2"],
                                      deleted=True,
                                      disabled=None)
        
        self.assertIsInstance(found_sites, dict)
        self.assertIsNone(found_sites["internal2"])
        self.assertEqual(found_sites["internal"], testSite)
        
        testSite.deleted = False
        testSite.disabled = True
        found_sites = lib.get_by_internal_ids_from_iterable(sites=sites,
                                      internal_ids=["internal", "internal2"],
                                      deleted=None,
                                      disabled=False)
        
        self.assertIsInstance(found_sites, dict)
        self.assertIsNone(found_sites["internal"])
        self.assertEqual(found_sites["internal2"], testSite2)
        
        found_sites = lib.get_by_internal_ids_from_iterable(sites=sites,
                                      internal_ids=["internal", "internal2"],
                                      deleted=None,
                                      disabled=True)
        
        self.assertIsInstance(found_sites, dict)
        self.assertIsNone(found_sites["internal2"])
        self.assertEqual(found_sites["internal"], testSite)
    
    
    def test_get_by_public_id_from_iterable(self):
        """
        @summary: Test getting a Site from a iterable with the specified
        public id.
        """
        
        testSite = Site()
        testSite.internal_id = "internal"
        testSite.public_id = "public"
        
        sites = set((testSite,))
        
        found_site = lib.get_by_public_id_from_iterable(sites=sites,
                                                      public_id="public",
                                                      deleted=None,
                                                      disabled=None)
        
        self.assertEqual(found_site, testSite)
        
        found_site = lib.get_by_public_id_from_iterable(sites=sites,
                                                      public_id="public2",
                                                      deleted=None,
                                                      disabled=None)
        
        self.assertIsNone(found_site)
        
        testSite.deleted = True
        found_site = lib.get_by_public_id_from_iterable(sites=sites,
                                                      public_id="public",
                                                      deleted=False,
                                                      disabled=None)
        
        self.assertIsNone(found_site)
        
        found_site = lib.get_by_public_id_from_iterable(sites=sites,
                                                      public_id="public",
                                                      deleted=True,
                                                      disabled=None)
        
        self.assertEqual(found_site, testSite)
        
        testSite.deleted = False
        testSite.disabled = True
        found_site = lib.get_by_public_id_from_iterable(sites=sites,
                                                      public_id="public",
                                                      deleted=None,
                                                      disabled=False)
        
        self.assertIsNone(found_site)
        
        found_site = lib.get_by_public_id_from_iterable(sites=sites,
                                                      public_id="public",
                                                      deleted=None,
                                                      disabled=True)
        
        self.assertEqual(found_site, testSite)
        
    
    def test_get_by_public_ids_from_iterable(self):
        """
        @summary: Test getting a Site from a iterable with the specified
        public ids.
        """
        
        testSite = Site()
        testSite.internal_id = "internal"
        testSite.public_id = "public"
        
        testSite2 = Site()
        testSite2.internal_id = "internal2"
        testSite2.public_id = "public2"
        
        sites = set((testSite, testSite2))
        
        found_sites = lib.get_by_public_ids_from_iterable(sites=sites,
                                      public_ids=["public", "public2"],
                                      deleted=None,
                                      disabled=None)
        
        self.assertIsInstance(found_sites, dict)
        self.assertEqual(found_sites["public"], testSite)
        self.assertEqual(found_sites["public2"], testSite2)
        
        found_sites = lib.get_by_public_ids_from_iterable(sites=sites,
                                      public_ids=["public3", "public4"],
                                      deleted=None,
                                      disabled=None)
        
        self.assertIsInstance(found_sites, dict)
        self.assertIsNone(found_sites["public3"])
        self.assertIsNone(found_sites["public4"])
        
        testSite.deleted = True
        
        found_sites = lib.get_by_public_ids_from_iterable(sites=sites,
                                      public_ids=["public", "public2"],
                                      deleted=False,
                                      disabled=None)
        
        self.assertIsInstance(found_sites, dict)
        self.assertIsNone(found_sites["public"])
        self.assertEqual(found_sites["public2"], testSite2)
        
        found_sites = lib.get_by_public_ids_from_iterable(sites=sites,
                                      public_ids=["public", "public2"],
                                      deleted=True,
                                      disabled=None)
        
        self.assertIsInstance(found_sites, dict)
        self.assertIsNone(found_sites["public2"])
        self.assertEqual(found_sites["public"], testSite)
        
        testSite.deleted = False
        testSite.disabled = True
        
        found_sites = lib.get_by_public_ids_from_iterable(sites=sites,
                                      public_ids=["public", "public2"],
                                      deleted=None,
                                      disabled=False)
        
        self.assertIsInstance(found_sites, dict)
        self.assertIsNone(found_sites["public"])
        self.assertEqual(found_sites["public2"], testSite2)
        
        found_sites = lib.get_by_public_ids_from_iterable(sites=sites,
                                      public_ids=["public", "public2"],
                                      deleted=None,
                                      disabled=True)
        
        self.assertIsInstance(found_sites, dict)
        self.assertIsNone(found_sites["public2"])
        self.assertEqual(found_sites["public"], testSite)
    
    
    def test_get_default_from_iterable(self):
        """
        @summary: Test getting the default site from an iterable of sites.
        """
        
        testSite = Site()
        testSite.internal_id = "internal"
        testSite.public_id = "public"
        testSite.default = True
        
        testSite2 = Site()
        testSite2.internal_id = "internal2"
        testSite2.public_id = "public2"
        
        sites = set((testSite, testSite2))
        
        found_site = lib.get_default_from_iterable(sites=sites,
                                                   deleted=None,
                                                   disabled=None)
        
        self.assertIsInstance(found_site, Site)
        
        testSite.deleted = True
        
        found_site = lib.get_default_from_iterable(sites=sites,
                                                   deleted=False,
                                                   disabled=None)
        
        self.assertIsNone(found_site)
        
        found_site = lib.get_default_from_iterable(sites=sites,
                                                   deleted=True,
                                                   disabled=None)
        
        self.assertIsInstance(found_site, Site)
        
        testSite.deleted = False
        testSite.disabled = True
        
        found_site = lib.get_default_from_iterable(sites=sites,
                                                   deleted=None,
                                                   disabled=False)
        
        self.assertIsNone(found_site)
        
        found_site = lib.get_default_from_iterable(sites=sites,
                                                   deleted=None,
                                                   disabled=True)
        
        self.assertIsInstance(found_site, Site)
        
        
    def test_no_db_implementation(self):
        """
        @summary: Calls all the methods that require a valid database
        implementation with something that doesn't implement a database
        """
        
        self.assertIsNone(lib.determine_db(connection=str))
        
        self.assertFalse(lib.update(connection=str, site=None))
        
        self.assertIsNone(lib.get_by_internal_id(connection=str,
                                                 internal_id="bob"))
        
        test_ids = ["bob"]
        
        found = lib.get_by_internal_ids(connection=str, internal_ids=test_ids)
        
        self.assertIsInstance(found, dict)
        
        for key, value in found.items():
            self.assertIn(key, test_ids)
            self.assertIsNone(value)
        
        self.assertIsNone(lib.get_by_public_id(connection=str, public_id="bob"))
        
        found = lib.get_by_public_ids(connection=str, public_ids=test_ids)
        
        self.assertIsInstance(found, dict)
        
        for key, value in found.items():
            self.assertIn(key, test_ids)
            self.assertIsNone(value)
        
        self.assertIsNone(lib.get_default(connection=str))