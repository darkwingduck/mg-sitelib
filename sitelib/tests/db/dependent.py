"""
Defines unit test for use with databases
"""


from __future__ import unicode_literals
import unittest
from sitelib.db.models import Site
from sitelib.db import lib


class DependentTestCase(unittest.TestCase):
    """
    @summary: Runs database specific test
    """
    
    
    def setUp(self):
        #database specific instances must define this object
        self.db = None
        
    
    def postSetUp(self):
        """
        @summary: Defines the actual setup of the testcase that isn't self.db
        specific.
        """
        
        self.testSite = Site()
        self.testSite.internal_id = "internal"
        self.testSite.public_id = "public"
        lib.update(connection=self.db, site=self.testSite)
    
    
    def test_create(self):
        """
        @summary: Test creating a site object
        """
        
        testSite = Site()
        
        self.assertTrue(lib.update(connection=self.db,
                                   site=testSite,
                                   _fail_first_generated_ids=True))
        
        testSite2 = Site()
        
        self.assertFalse(lib.update(connection=self.db,
                                   site=testSite2,
                                   generate_internal_id=None))
    
    
    def test_operators(self):
        """
        @summary: Test the operator functions for the model
        """
        
        testSite = Site()
        
        self.assertTrue(lib.update(connection=self.db,
                                   site=testSite,
                                   _fail_first_generated_ids=True))
        
        testSite2 = Site()
        
        self.assertFalse(lib.update(connection=self.db,
                                   site=testSite2,
                                   generate_internal_id=None))
        
        self.assertFalse(testSite == testSite2)
        self.assertTrue(testSite != testSite2)
    
    
    def test_get_by_internal_id(self):
        """
        @summary: Test getting a site by its internal id.
        """
        
        site = lib.get_by_internal_id(connection=self.db,
                                        internal_id="internal",
                                        deleted=None,
                                        disabled=None)
        
        self.assertIsInstance(site, Site)
        self.assertEqual(site.public_id, "public")
        
        site.deleted = True
        lib.update(connection=self.db, site=site)
        
        site = lib.get_by_internal_id(connection=self.db,
                                        internal_id="internal",
                                        deleted=False,
                                        disabled=None)
        
        self.assertIsNone(site)
        
        site = lib.get_by_internal_id(connection=self.db,
                                        internal_id="internal",
                                        deleted=True,
                                        disabled=None)
        
        self.assertIsInstance(site, Site)
        self.assertEqual(site.public_id, "public")
        
        site.deleted = False
        site.disabled = True
        lib.update(connection=self.db, site=site)
        
        site = lib.get_by_internal_id(connection=self.db,
                                        internal_id="internal",
                                        deleted=None,
                                        disabled=False)
        
        self.assertIsNone(site)
        
        site = lib.get_by_internal_id(connection=self.db,
                                        internal_id="internal",
                                        deleted=None,
                                        disabled=True)
        
        self.assertIsInstance(site, Site)
        self.assertEqual(site.public_id, "public")
    
    
    def test_get_by_internal_ids(self):
        """
        @summary: Test getting sites by their internal ids.
        """
        
        testSite = Site()
        testSite.internal_id = "internal2"
        testSite.public_id = "public2"
        lib.update(connection=self.db, site=testSite)
        
        sites = lib.get_by_internal_ids(connection=self.db,
                                          internal_ids=("internal", "internal2"),
                                          deleted=None,
                                          disabled=None)
        
        self.assertEqual(len(sites), 2)
        self.assertIsInstance(sites["internal"], Site)
        self.assertIsInstance(sites["internal2"], Site)
        self.assertEqual(sites["internal"].public_id, "public")
        self.assertEqual(sites["internal2"].public_id, "public2")
        
        testSite.deleted = True
        lib.update(connection=self.db, site=testSite)
        
        sites = lib.get_by_internal_ids(connection=self.db,
                                          internal_ids=("internal", "internal2"),
                                          deleted=False,
                                          disabled=None)
        
        self.assertEqual(len(sites), 2)
        self.assertIsInstance(sites["internal"], Site)
        self.assertIsNone(sites["internal2"])
        self.assertEqual(sites["internal"].public_id, "public")
        
        sites = lib.get_by_internal_ids(connection=self.db,
                                          internal_ids=("internal", "internal2"),
                                          deleted=True,
                                          disabled=None)
        
        self.assertEqual(len(sites), 2)
        self.assertIsNone(sites["internal"])
        self.assertIsInstance(sites["internal2"], Site)
        self.assertEqual(sites["internal2"].public_id, "public2")
        
        testSite.deleted = False
        testSite.disabled = True
        lib.update(connection=self.db, site=testSite)
        
        sites = lib.get_by_internal_ids(connection=self.db,
                                          internal_ids=("internal", "internal2"),
                                          deleted=None,
                                          disabled=False)
        
        self.assertEqual(len(sites), 2)
        self.assertIsInstance(sites["internal"], Site)
        self.assertIsNone(sites["internal2"])
        self.assertEqual(sites["internal"].public_id, "public")
        
        sites = lib.get_by_internal_ids(connection=self.db,
                                          internal_ids=("internal", "internal2"),
                                          deleted=None,
                                          disabled=True)
        
        self.assertEqual(len(sites), 2)
        self.assertIsNone(sites["internal"])
        self.assertIsInstance(sites["internal2"], Site)
        self.assertEqual(sites["internal2"].public_id, "public2")
    
    
    def test_get_by_public_id(self):
        """
        @summary: Test getting a site by its public id.
        """
        
        site = lib.get_by_public_id(connection=self.db,
                                      public_id="public",
                                      deleted=None,
                                      disabled=None)
        
        self.assertIsInstance(site, Site)
        self.assertEqual(site.internal_id, "internal")
        
        site.deleted = True
        lib.update(connection=self.db, site=site)
        
        site = lib.get_by_public_id(connection=self.db,
                                      public_id="public",
                                      deleted=False,
                                      disabled=None)
        
        self.assertIsNone(site)
        
        site = lib.get_by_public_id(connection=self.db,
                                      public_id="public",
                                      deleted=True,
                                      disabled=None)
        
        self.assertIsInstance(site, Site)
        self.assertEqual(site.internal_id, "internal")
        
        site.deleted = False
        site.disabled = True
        lib.update(connection=self.db, site=site)
        
        site = lib.get_by_public_id(connection=self.db,
                                      public_id="public",
                                      deleted=None,
                                      disabled=False)
        
        self.assertIsNone(site)
        
        site = lib.get_by_public_id(connection=self.db,
                                      public_id="public",
                                      deleted=None,
                                      disabled=True)
        
        self.assertIsInstance(site, Site)
        self.assertEqual(site.internal_id, "internal")
    
    
    def test_get_by_public_ids(self):
        """
        @summary: Test getting sites by their public ids.
        """
        
        testSite = Site()
        testSite.internal_id = "internal2"
        testSite.public_id = "public2"
        lib.update(connection=self.db, site=testSite)
        
        sites = lib.get_by_public_ids(connection=self.db,
                                        public_ids=("public", "public2"),
                                        deleted=None,
                                        disabled=None)
        
        self.assertEqual(len(sites), 2)
        self.assertIsInstance(sites["public"], Site)
        self.assertIsInstance(sites["public2"], Site)
        self.assertEqual(sites["public"].internal_id, "internal")
        self.assertEqual(sites["public2"].internal_id, "internal2")
        
        testSite.deleted = True
        lib.update(connection=self.db, site=testSite)
        
        sites = lib.get_by_public_ids(connection=self.db,
                                        public_ids=("public", "public2"),
                                        deleted=False,
                                        disabled=None)
        
        self.assertEqual(len(sites), 2)
        self.assertIsInstance(sites["public"], Site)
        self.assertIsNone(sites["public2"])
        self.assertEqual(sites["public"].internal_id, "internal")
        
        sites = lib.get_by_public_ids(connection=self.db,
                                        public_ids=("public", "public2"),
                                        deleted=True,
                                        disabled=None)
        
        self.assertEqual(len(sites), 2)
        self.assertIsNone(sites["public"])
        self.assertIsInstance(sites["public2"], Site)
        self.assertEqual(sites["public2"].internal_id, "internal2")
        
        testSite.deleted = False
        testSite.disabled = True
        lib.update(connection=self.db, site=testSite)
        
        sites = lib.get_by_public_ids(connection=self.db,
                                        public_ids=("public", "public2"),
                                        deleted=None,
                                        disabled=False)
        
        self.assertEqual(len(sites), 2)
        self.assertIsInstance(sites["public"], Site)
        self.assertIsNone(sites["public2"])
        self.assertEqual(sites["public"].internal_id, "internal")
        
        sites = lib.get_by_public_ids(connection=self.db,
                                        public_ids=("public", "public2"),
                                        deleted=None,
                                        disabled=True)
        
        self.assertEqual(len(sites), 2)
        self.assertIsNone(sites["public"])
        self.assertIsInstance(sites["public2"], Site)
        self.assertEqual(sites["public2"].internal_id, "internal2")
    
    
    def test_get_default(self):
        """
        @summary: Test getting the default site
        """
        
        testSite = Site()
        testSite.internal_id = "internal2"
        testSite.public_id = "public2"
        testSite.default = True
        lib.update(connection=self.db, site=testSite)
        
        default = lib.get_default(connection=self.db, deleted=None, disabled=None)
        
        self.assertIsInstance(default, Site)
        self.assertEqual(default.internal_id, "internal2")
        
        testSite.deleted = True
        lib.update(connection=self.db, site=testSite)
        
        default = lib.get_default(connection=self.db, deleted=False, disabled=None)
        
        self.assertIsNone(default)
        
        default = lib.get_default(connection=self.db, deleted=True, disabled=None)
        
        self.assertIsInstance(default, Site)
        self.assertEqual(default.internal_id, "internal2")
        
        testSite.deleted = False
        testSite.disabled = True
        lib.update(connection=self.db, site=testSite)
        
        default = lib.get_default(connection=self.db, deleted=None, disabled=False)
        
        self.assertIsNone(default)
        
        default = lib.get_default(connection=self.db, deleted=None, disabled=True)
        
        self.assertIsInstance(default, Site)
        self.assertEqual(default.internal_id, "internal2")
    
    
    def test_get_domains_for_site(self):
        """
        @summary: Test getting the domains for a given site.
        
        If domainlib is not installed, this test will not be run.
        """
        
        dblib = None
        Domain = None
        
        try:
            from domainlib.db import lib as dblib
            from domainlib.db.models import Domain
                        
        except Exception as dummy:
            pass
        
        if dblib and Domain:
            domain1 = Domain()
            domain1.owner_id = self.testSite.internal_id
            domain1.root_zone = "example.com"
            domain1.default = False
            domain1.domain.root_zone = r"^example\.com$"
            domain1.domain.sub_domain = r"^\w*$"
            domain1.domain.weight = 0.0
            dblib.update(connection=self.db, domain=domain1)
            
            #forcing import error
            domains = lib.get_domains_for_site(connection=self.db,
                                               site=self.testSite,
                                               deleted=None,
                                               disabled=None,
                                               _force_import_error=True)
            
            self.assertIsInstance(domains, set)
            
            domains = lib.get_domains_for_site(connection=self.db,
                                               site=self.testSite,
                                               deleted=None,
                                               disabled=None)
            
            self.assertIsInstance(domains, set)
            self.assertEqual(len(domains), 1)
            
            domain1.deleted = True
            dblib.update(connection=self.db, domain=domain1)
            
            domains = lib.get_domains_for_site(connection=self.db,
                                               site=self.testSite,
                                               deleted=False,
                                               disabled=None)
            
            self.assertIsInstance(domains, set)
            self.assertEqual(len(domains), 0)
            
            dblib.update(connection=self.db, domain=domain1)
            
            domains = lib.get_domains_for_site(connection=self.db,
                                               site=self.testSite,
                                               deleted=True,
                                               disabled=None)
            
            self.assertIsInstance(domains, set)
            self.assertEqual(len(domains), 1)
            
            domain1.deleted = False
            domain1.disabled = True
            dblib.update(connection=self.db, domain=domain1)
            
            domains = lib.get_domains_for_site(connection=self.db,
                                               site=self.testSite,
                                               deleted=None,
                                               disabled=False)
            
            self.assertIsInstance(domains, set)
            self.assertEqual(len(domains), 0)
            
            dblib.update(connection=self.db, domain=domain1)
            
            domains = lib.get_domains_for_site(connection=self.db,
                                               site=self.testSite,
                                               deleted=None,
                                               disabled=True)
            
            self.assertIsInstance(domains, set)
            self.assertEqual(len(domains), 1)

    
    
    def test_get_default_domain_for_site(self):
        """
        @summary: Test getting the default domain for a site.
        
        If domainlib is not installed, this test will not be run.
        """
        
        dblib = None
        Domain = None
        
        try:
            from domainlib.db import lib as dblib
            from domainlib.db.models import Domain
                    
        except Exception as dummy:
            pass
        
        if dblib and Domain:
            domain1 = Domain()
            domain1.owner_id = self.testSite.internal_id
            domain1.root_zone = "example.com"
            domain1.default = True
            domain1.domain.root_zone = r"^example\.com$"
            domain1.domain.sub_domain = r"^\w*$"
            domain1.domain.weight = 0.0
            dblib.update(connection=self.db, domain=domain1)
            
            #forcing import error
            domain = lib.get_default_domain_for_site(connection=self.db,
                                                     site=self.testSite,
                                                     deleted=None,
                                                     disabled=None,
                                                     _force_import_error=True)
            
            self.assertIsNone(domain)
            
            domain = lib.get_default_domain_for_site(connection=self.db,
                                                     site=self.testSite,
                                                     deleted=None,
                                                     disabled=None)
            
            self.assertIsInstance(domain, Domain)
            self.assertEqual(domain.internal_id, domain1.internal_id)
            
            domain1.deleted = True
            dblib.update(connection=self.db, domain=domain1)
            
            domain = lib.get_default_domain_for_site(connection=self.db,
                                                     site=self.testSite,
                                                     deleted=False,
                                                     disabled=None)
            
            self.assertIsNone(domain)
            
            domain = lib.get_default_domain_for_site(connection=self.db,
                                                     site=self.testSite,
                                                     deleted=True,
                                                     disabled=None)
            
            self.assertIsInstance(domain, Domain)
            self.assertEqual(domain.internal_id, domain1.internal_id)
            
            domain1.deleted = False
            domain1.disabled = True
            dblib.update(connection=self.db, domain=domain1)
            
            domain = lib.get_default_domain_for_site(connection=self.db,
                                                     site=self.testSite,
                                                     deleted=None,
                                                     disabled=False)
            
            self.assertIsNone(domain)
            
            domain = lib.get_default_domain_for_site(connection=self.db,
                                                     site=self.testSite,
                                                     deleted=None,
                                                     disabled=True)
            
            self.assertIsInstance(domain, Domain)
            self.assertEqual(domain.internal_id, domain1.internal_id)
    
    
    def test_get_site_using_host(self):
        """
        @summary: Test getting a site instance from a specified host.
        
        If domainlib is not installed, this test will not be run.
        """
        
        dblib = None
        Domain = None
        
        try:
            from domainlib.db import lib as dblib
            from domainlib.db.models import Domain
                        
        except Exception as dummy:
            pass
        
        if dblib and Domain:
            testSite2 = Site()
            testSite2.internal_id = "internal2"
            testSite2.public_id = "public2"
            lib.update(connection=self.db, site=testSite2)
            
            testSite3 = Site()
            testSite3.internal_id = "internal3"
            testSite3.public_id = "public3"
            lib.update(connection=self.db, site=testSite3)
            
            testSite4 = Site()
            testSite4.internal_id = "internal4"
            testSite4.public_id = "public4"
            lib.update(connection=self.db, site=testSite4)
        
            domain1 = Domain()
            domain1.owner_id = self.testSite.internal_id
            domain1.root_zone = "example.com"
            domain1.default = True
            domain1.domain.root_zone = r"^example\.com$"
            domain1.domain.sub_domain = r"^\w*$"
            domain1.domain.weight = 0.0
            dblib.update(connection=self.db, domain=domain1)
            
            domain2 = Domain()
            domain2.owner_id = testSite2.internal_id
            domain2.root_zone = "example.com"
            domain2.default = False
            domain2.domain.root_zone = r"^example\.com$"
            domain2.domain.sub_domain = r"(^\w+\.|^)images$"
            domain2.domain.weight = 0.1
            dblib.update(connection=self.db, domain=domain2)
            
            domain3 = Domain()
            domain3.owner_id = testSite3.internal_id
            domain3.root_zone = "google.com"
            domain3.default = True
            domain3.domain.root_zone = r"^google\.com$"
            domain3.domain.sub_domain = r"^\w*$"
            domain3.domain.weight = 0.0
            dblib.update(connection=self.db, domain=domain3)
            
            domain4 = Domain()
            domain4.owner_id = testSite4.internal_id
            domain4.root_zone = "google.com"
            domain4.default = False
            domain4.domain.root_zone = r"^google\.com$"
            domain4.domain.sub_domain = r"(^\w+\.|^)images$"
            domain4.domain.weight = 0.1
            dblib.update(connection=self.db, domain=domain4)
            
            #forcing import error
            site = lib.get_site_using_host(connection=self.db,
                                           host="example.com",
                                           site_deleted=None,
                                           site_disabled=None,
                                           domain_deleted=None,
                                           domain_disabled=None,
                                           _force_import_error=True)
            
            self.assertIsNone(site)
            
            site = lib.get_site_using_host(connection=self.db,
                                           host="example.com",
                                           site_deleted=None,
                                           site_disabled=None,
                                           domain_deleted=None,
                                           domain_disabled=None)
            
            self.assertIsInstance(site, Site)
            self.assertEqual(site.internal_id, self.testSite.internal_id)
            
            site = lib.get_site_using_host(connection=self.db,
                                           host="www.example.com",
                                           site_deleted=None,
                                           site_disabled=None,
                                           domain_deleted=None,
                                           domain_disabled=None)
            
            self.assertIsInstance(site, Site)
            self.assertEqual(site.internal_id, self.testSite.internal_id)
            
            site = lib.get_site_using_host(connection=self.db,
                                           host="images.example.com",
                                           site_deleted=None,
                                           site_disabled=None,
                                           domain_deleted=None,
                                           domain_disabled=None)
            
            self.assertIsInstance(site, Site)
            self.assertEqual(site.internal_id, testSite2.internal_id)
            
            self.testSite.default = True
            lib.update(connection=self.db, site=self.testSite)
            
            site = lib.get_site_using_host(connection=self.db,
                                           host="flappy-paddle-gearbox.com",
                                           site_deleted=None,
                                           site_disabled=None,
                                           domain_deleted=None,
                                           domain_disabled=None)
            
            self.assertEqual(site, self.testSite)
    
    
    def test_get_all(self):
        """
        @summary: Test getting all of the sites in the database.
        """
        
        testSite2 = Site()
        testSite2.internal_id = "internal2"
        testSite2.public_id = "public2"
        lib.update(connection=self.db, site=testSite2)
        
        testSite3 = Site()
        testSite3.internal_id = "internal3"
        testSite3.public_id = "public3"
        lib.update(connection=self.db, site=testSite3)
        
        sites = lib.get_all(connection=self.db, deleted=None, disabled=None)
        self.assertIsInstance(sites, set)
        self.assertEqual(len(sites), 3)
        
        expected = [self.testSite, testSite2, testSite3]
        
        for site in sites:
            self.assertIn(site, expected)
        
        testSite3.deleted = True
        lib.update(connection=self.db, site=testSite3)
        
        sites = lib.get_all(connection=self.db, deleted=False, disabled=None)
        self.assertEqual(len(sites), 2)
        
        expected = [self.testSite, testSite2]
        
        for site in sites:
            self.assertIn(site, expected)
        
        sites = lib.get_all(connection=self.db, deleted=True, disabled=None)
        self.assertIsInstance(sites, set)
        self.assertEqual(len(sites), 1)
        
        expected = [testSite3]
        
        for site in sites:
            self.assertIn(site, expected)
        
        testSite3.deleted = False
        testSite3.disabled = True
        lib.update(connection=self.db, site=testSite3)
        
        sites = lib.get_all(connection=self.db, deleted=None, disabled=False)
        self.assertEqual(len(sites), 2)
        
        expected = [self.testSite, testSite2]
        
        for site in sites:
            self.assertIn(site, expected)
        
        sites = lib.get_all(connection=self.db, deleted=None, disabled=True)
        self.assertIsInstance(sites, set)
        self.assertEqual(len(sites), 1)
        
        expected = [testSite3]
        
        for site in sites:
            self.assertIn(site, expected)
    
    
    def test_default(self):
        """
        @summary: Test setting the site as default and marking all other sites
        as not default.
        """
        
        testSite2 = Site()
        testSite2.default = True
        testSite2.internal_id = "internal2"
        testSite2.public_id = "public2"
        lib.update(connection=self.db, site=testSite2)
        
        testSite3 = Site()
        testSite3.internal_id = "internal3"
        testSite3.public_id = "public3"
        lib.update(connection=self.db, site=testSite3)
        
        self.assertTrue(lib.default(connection=self.db,
                                    site=testSite3,
                                    _fail_implementation=False,
                                    _raise_implementation_exception=False,
                                    _fail_non_impl_defalt=False))
        
        default = lib.get_default(connection=self.db,
                                  deleted=None,
                                  disabled=None)
        
        self.assertEqual(default, testSite3)
        
        self.assertTrue(lib.default(connection=self.db,
                                     site=testSite3,
                                     _fail_implementation=True,
                                     _raise_implementation_exception=False,
                                     _fail_non_impl_defalt=False))

        self.assertFalse(lib.default(connection=self.db,
                                     site=testSite3,
                                     _fail_implementation=True,
                                     _raise_implementation_exception=False,
                                     _fail_non_impl_defalt=True))
        
        testSite2.deleted = True
        lib.update(connection=self.db, site=testSite2)
        
        self.assertFalse(lib.default(connection=self.db,
                                     site=testSite2,
                                     _fail_implementation=False,
                                     _raise_implementation_exception=False,
                                     _fail_non_impl_defalt=False))
    
    
    def test_disable(self):
        """
        @summary: Test disabling a domain that is currently default and setting
        a new domain to be the default.
        """
        
        testSite1 = Site()
        testSite1.default = True
        testSite1.internal_id = "internal2"
        testSite1.public_id = "public2"
        lib.update(connection=self.db, site=testSite1)
        
        testSite2 = Site()
        testSite2.internal_id = "internal3"
        testSite2.public_id = "public3"
        lib.update(connection=self.db, site=testSite2)
        
        default_results = lib.disable(connection=self.db,
                                      site=testSite1,
                                      new_default=testSite2)
        
        self.assertTrue(default_results)
        
        sites = lib.get_all(connection=self.db, deleted=None, disabled=None)
        
        for site in sites:
            if site.default:
                self.assertEqual(site, testSite2)
            
            elif site == testSite1:
                self.assertTrue(site.disabled)
        
        #disabling an already disabled site
        default_results = lib.disable(connection=self.db,
                                      site=testSite1,
                                      new_default=testSite2)
        
        self.assertTrue(default_results)
        
        #making it fail because the new default site will be disabled
        default_results = lib.disable(connection=self.db,
                                      site=testSite2,
                                      new_default=testSite1)
        
        self.assertFalse(default_results)
        
        testSite1.default = True
        testSite1.disabled = False
        lib.update(connection=self.db, site=testSite1)
        
        testSite2.default = False
        testSite2.disabled = False
        lib.update(connection=self.db, site=testSite2)
        
        #failing the default call to make testSite1 remains enabled and
        #default
        default_results = lib.disable(connection=self.db,
                                      site=testSite1,
                                      new_default=testSite2,
                                      _fail_default=True)
        
        self.assertFalse(default_results)
        
        sites = lib.get_all(connection=self.db, deleted=None, disabled=None)
        
        for site in sites:
            if site.default:
                self.assertEqual(site, testSite1)
            
            elif site == testSite2:
                self.assertFalse(site.default)
    
    
    def test_delete(self):
        """
        @summary: Test deleting a site that is currently default and setting
        a new site to be the default.
        """
        
        testSite1 = Site()
        testSite1.default = True
        testSite1.internal_id = "internal2"
        testSite1.public_id = "public2"
        lib.update(connection=self.db, site=testSite1)
        
        testSite2 = Site()
        testSite2.internal_id = "internal3"
        testSite2.public_id = "public3"
        lib.update(connection=self.db, site=testSite2)
        
        default_results = lib.delete(connection=self.db,
                                     site=testSite1,
                                     new_default=testSite2)
        
        self.assertTrue(default_results)
        
        sites = lib.get_all(connection=self.db, deleted=None, disabled=None)
        
        for site in sites:
            if site.default:
                self.assertEqual(site, testSite2)
            
            elif site == testSite1:
                self.assertTrue(site.deleted)
        
        #deleteing an already deleted site
        default_results = lib.delete(connection=self.db,
                                      site=testSite1,
                                      new_default=testSite2)
        
        self.assertTrue(default_results)
        
        #making it fail because the new default site will be disabled
        default_results = lib.delete(connection=self.db,
                                     site=testSite2,
                                     new_default=testSite1)
        
        self.assertFalse(default_results)
        
        testSite1.default = True
        testSite1.deleted = False
        lib.update(connection=self.db, site=testSite1)
        
        testSite2.default = False
        testSite2.deleted = False
        lib.update(connection=self.db, site=testSite2)
        
        #failing the default call to make testSite1 remains enabled and
        #default
        default_results = lib.delete(connection=self.db,
                                     site=testSite1,
                                     new_default=testSite2,
                                     _fail_default=True)
        
        self.assertFalse(default_results)
        
        sites = lib.get_all(connection=self.db, deleted=None, disabled=None)
        
        for site in sites:
            if site.default:
                self.assertEqual(site, testSite1)
            
            elif site == testSite2:
                self.assertFalse(site.deleted)