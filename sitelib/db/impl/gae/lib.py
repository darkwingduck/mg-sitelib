"""
Defines the google app engine specific Site library
"""


from google.appengine.ext import db
from sitelib.db.models import Site
from sitelib.db.impl.gae.models import Site as SiteModel
import json
from datetime import datetime
import logging

log = logging.getLogger(__name__)


def _model_to_site(result):
    """
    @summary: Takes the result from the database and loads its information into
    the abstracted sitelib.db.models.Site.
    
    @type result: sitelib.db.impl.gae.models.Site
    @param result: Row from the query being converted.
    
    @rtype: sitelib.db.models.Site
    @return: Site instance with loaded data or None if data could not be 
    loaded or is a version greater than this library can load.
    """
    
    if result:
        site = Site()
        site._key = result
        site.internal_id = result.internal_id
        site.public_id = result.public_id
        site.default = result.default
        site.created = result.created
        site.updated = result.updated
        site.disabled = result.disabled
        site.deleted = result.deleted
        
        if result.non_indexed_version == 1:
            data = json.loads(result.non_indexed_data)
            
            return _model_to_site_v_1(site=site, data=data)
    
    return None


def _model_to_site_v_1(site, data):
    """
    @summary: Loads the data from a result for version 1 of the non_indexed_data
    structure.
    
    @type site: sitelib.db.models.Site
    @param site: Site instance data is being loaded into.
    
    @type data: Dict
    @param data: Dictionary holding the non-indexed data.
    
    @rtype: sitelib.db.models.Site
    @return: Site instance with loaded data.
    """
    
    site.name = data.get("name", "")
    site.description = data.get("description", "")
    
    return site


def _site_to_model(connection, site, generate_internal_id,
                     validate_internal_id, generate_public_id,
                     validate_public_id, _fail_first_internal_id=False,
                     _fail_first_public_id=False):
    
    """
    @summary: Takes the sitelib.db.models.Site and loads its information 
    into a model for the database.  The non_indexed_data will be written as the
    latest version.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type site: sitelib.db.models.Site
    @param site: Site instance with data to save.

    @type site: sitelib.db.models.Site
    @param site: Site instance being saved to the database.  If the site
    does not have a _key, it will be created.
    
    @type generate_internal_id: Function
    @param generate_internal_id: Function used to generate a unique internal
    id for created sites.  This function takes no arguments and should return
    a unicode of 500 characters or less.
    
    This function will only be called if the internal_id has not been set yet.
    
    @type validate_internal_id: Function
    @param validate_internal_id: Function used to validate that a generated
    internal id is unique.  This function should take a connection and unicode
    and return True if the id is unique and False if it is not unique.
    
    This function will only be called if the internal_id has not been set yet.
    
    @type generate_public_id: Function
    @param generate_public_id: Function used to generate a unique public
    id for created sites.  This function takes no arguments and should return
    a unicode of 500 characters or less.
    
    This function will only be called if the public_id has not been set yet.
    
    @type validate_public_id: Function
    @param validate_public_id: Function used to validate that a generated
    public id is unique.  This function should take a connection and unicode
    and return True if the id is unique and False if it is not unique.
    
    This function will only be called if the public_id has not been set yet.
    
    @type _fail_first_internal_id: Boolean
    @param _fail_first_internal_id: Whether the first generated internal id
    should be treated as invalid.  This is only used for unittesting.
    
    @type _fail_first_public_id: Boolean
    @param _fail_first_public_id: Whether the first generated public id should
    be treated as invalid.  This is only used for unittesting.
    
    @rtype: sitelib.db.impl.gae.models.Site
    @return: Row from the query being updated.
    """
    
    model = site._key
    
    non_indexed_data = json.dumps(_site_to_model_v_1(site=site))
    
    #creating the instance
    if not model:
        if not site.internal_id:
            site.internal_id = generate_internal_id()
            
            while (not validate_internal_id(connection, site.internal_id)
                   or _fail_first_internal_id):
                
                if _fail_first_internal_id:
                    _fail_first_internal_id = False
                
                site.internal_id = generate_internal_id()
        
        if not site.public_id:
            site.public_id = generate_public_id()
            
            while (not validate_public_id(connection, site.public_id) or
                   _fail_first_public_id):
                
                if _fail_first_public_id:
                    _fail_first_public_id = False
                
                site.public_id = generate_public_id()
        
        site.created = datetime.now()
        site.updated = datetime.now()
        
        model = SiteModel(
            internal_id=site.internal_id,
            public_id=site.public_id,
            default=site.default,
            created=site.created,
            updated=site.updated,
            disabled=site.disabled,
            deleted=site.deleted,
            non_indexed_version=SiteModel.CURRENT_NON_INDEXED_DATA_VERSION,
            non_indexed_data=non_indexed_data)
    
    else:
        site.updated = datetime.now()
        
        model.internal_id = site.internal_id
        model.public_id = site.public_id
        model.default = site.default
        model.created = site.created
        model.updated = site.updated
        model.disabled = site.disabled
        model.deleted = site.deleted
        model.non_indexed_version = SiteModel.CURRENT_NON_INDEXED_DATA_VERSION
        model.non_indexed_data = non_indexed_data
    
    return model


def _site_to_model_v_1(site):
    """
    @summary: Loads the data from a result for version 1 of the non_indexed_data
    structure.
    
    @type site: sitelib.db.models.Site
    @param site: Site instance data is being saved from.
    
    @rtype: Dict
    @return: Dictionary holding the non-indexed data.
    """
    
    return {
        "name" : site.name,
        "description" : site.description
    }


def update(connection, site, generate_internal_id, validate_internal_id,
           generate_public_id, validate_public_id,
           _fail_first_generated_ids=False):
    
    """
    @summary: Updates a Site instance to the connected database.  The instance
    must exists before updating.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type site: sitelib.db.models.Site
    @param site: Site instance being saved to the database.  If the site
    does not have a _key, it will be created.
    
    If the site was created, it's internal_id and public_id will be created
    if they were not previously specified.
    
    @type site: sitelib.db.models.Site
    @param site: Site instance being saved to the database.  If the site
    does not have a _key, it will be created.
    
    @type generate_internal_id: Function
    @param generate_internal_id: Function used to generate a unique internal
    id for created sites.  This function takes no arguments and should return
    a unicode of 500 characters or less.
    
    This function will only be called if the internal_id has not been set yet.
    
    @type validate_internal_id: Function
    @param validate_internal_id: Function used to validate that a generated
    internal id is unique.  This function should take a connection and unicode
    and return True if the id is unique and False if it is not unique.
    
    This function will only be called if the internal_id has not been set yet.
    
    @type generate_public_id: Function
    @param generate_public_id: Function used to generate a unique public
    id for created sites.  This function takes no arguments and should return
    a unicode of 500 characters or less.
    
    This function will only be called if the public_id has not been set yet.
    
    @type validate_public_id: Function
    @param validate_public_id: Function used to validate that a generated
    public id is unique.  This function should take a connection and unicode
    and return True if the id is unique and False if it is not unique.
    
    This function will only be called if the public_id has not been set yet.
    
    @type _fail_first_generated_ids: Boolean
    @param _fail_first_generated_ids: Whether to fail the first generated
    internal and public ids.  This is only used by unittests.
    
    @rtype: Boolean
    @return: Whether the instance could be saved or not.
    """
    
    key = None
    
    try:
        model = _site_to_model(connection=connection,
                               site=site,
                               generate_internal_id=generate_internal_id,
                               validate_internal_id=validate_internal_id,
                               generate_public_id=generate_public_id,
                               validate_public_id=validate_public_id,
                               _fail_first_internal_id=_fail_first_generated_ids,
                               _fail_first_public_id=_fail_first_generated_ids)
        
        key = model.put()
    
    except Exception as e:
        log.error("Error updating Site with Google App Engine:%s" % e)
    
    if key:
        if not site._key:
            site._key = model
        
        return True
    
    return False


def get_by_internal_id(connection, internal_id, deleted, disabled):
    """
    @summary: Gets a Site from the database with the specified internal_id.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type internal_id: Unicode
    @param internal_id: Internal id of the site to be retrieved.
    
    @type deleted: Boolean
    @param deleted: Whether the Site has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Site is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @rtype: sitelib.db.models.Site
    @return: Loaded Site model or None of not found.
    """
    
    query = db.Query(SiteModel).filter("internal_id ==", internal_id)
    
    if deleted is not None:
        query = query.filter("deleted ==", deleted)
    
    if disabled is not None:
        query = query.filter("disabled ==", disabled)
    
    result = None
    
    for r in query.fetch(1):
        result = r
    
    return _model_to_site(result=result)


def get_by_internal_ids(connection, internal_ids, deleted, disabled):
    """
    @summary: Gets Sites from the database with the specified internal_id.
    
    This is done as a single query on all entities because we want to run one
    query rather than one query for each id.  GAE's IN operator performs one
    query per entry in the iterable up to 30 entries.  If we need more than 30,
    we would have to perform the filter manually so we may as well do it.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type internal_ids: Iterable
    @param internal_ids: Iterable of internal ids of the site to be retrieved.
    
    @type deleted: Boolean
    @param deleted: Whether the Site has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Site is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @rtype: Dict
    @return: Dictionary mapping of Sites with the internal id as key.  If no
    site could be loaded, it's value will be None.
    """
    
    query = db.Query(SiteModel)
    
    if deleted is not None:
        query = query.filter("deleted ==", deleted)
    
    if disabled is not None:
        query = query.filter("disabled ==", disabled)
    
    ids = dict.fromkeys(internal_ids)
    
    results = filter(lambda x:x.internal_id in internal_ids, map(None, query))
    
    for result in results:
        ids[result.internal_id] = _model_to_site(result)
    
    return ids


def get_by_public_id(connection, public_id, deleted, disabled):
    """
    @summary: Gets a Site from the database with the specified public_id.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type public_id: Unicode
    @param public_id: Public id of the site to be retrieved.
    
    @type deleted: Boolean
    @param deleted: Whether the Site has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Site is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @rtype: sitelib.db.models.Site
    @return: Loaded Site model or None of not found.
    """
    
    query = db.Query(SiteModel).filter("public_id ==", public_id)
    
    if deleted is not None:
        query = query.filter("deleted ==", deleted)
    
    if disabled is not None:
        query = query.filter("disabled ==", disabled)
    
    result = None
    
    for r in query.fetch(1):
        result = r
    
    return _model_to_site(result=result)


def get_by_public_ids(connection, public_ids, deleted, disabled):
    """
    @summary: Gets Sites from the database with the specified public_id.
    
    This is done as a single query on all entities because we want to run one
    query rather than one query for each id.  GAE's IN operator performs one
    query per entry in the iterable up to 30 entries.  If we need more than 30,
    we would have to perform the filter manually so we may as well do it.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type public_ids: Iterable
    @param public_ids: Iterable of public ids of the site to be retrieved.
    
    @type deleted: Boolean
    @param deleted: Whether the Site has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Site is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @rtype: Dict
    @return: Dictionary mapping of Sites with the public id as key.  If no
    site could be loaded, it's value will be None.
    """
    
    query = db.Query(SiteModel)
    
    if deleted is not None:
        query = query.filter("deleted ==", deleted)
    
    if disabled is not None:
        query = query.filter("disabled ==", disabled)
    
    ids = dict.fromkeys(public_ids)
    
    results = filter(lambda x:x.public_id in public_ids, map(None, query))
    
    for result in results:
        ids[result.public_id] = _model_to_site(result)
    
    return ids


def get_default(connection, deleted=None, disabled=None):
    """
    @summary: Gets the default Site from the database.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type deleted: Boolean
    @param deleted: Whether the Site has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Site is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @rtype: sitelib.db.models.Site
    @return: Loaded Site model or None of not found.
    """
    
    query = db.Query(SiteModel).filter("default ==", True)
    
    if deleted is not None:
        query = query.filter("deleted ==", deleted)
    
    if disabled is not None:
        query = query.filter("disabled ==", disabled)
    
    result = None
    
    for r in query.fetch(1):
        result = r
    
    return _model_to_site(result=result)


def get_all(connection, deleted=None, disabled=None):
    """
    @summary: Gets all the sites.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type deleted: Boolean
    @param deleted: Whether the Site has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Site is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @rtype: Set
    @return: Set containing all loaded sites.
    """
    
    query = db.Query(SiteModel)
    
    if deleted is not None:
        query = query.filter("deleted ==", deleted)
    
    if disabled is not None:
        query = query.filter("disabled ==", disabled)
    
    sites = set()

    for result in query:
        sites.add(_model_to_site(result))
    
    return sites