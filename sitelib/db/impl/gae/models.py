"""
Defines the google app engine specific models.
"""


from google.appengine.ext import db


class Site(db.Model):
    """
    @summary: Defines the database independent Site model that each database
    will load data into and save data from.
    
    @type CURRENT_NON_INDEXED_DATA_VERSION: Integer
    @cvar CURRENT_NON_INDEXED_DATA_VERSION: Integer representing the current
    version of the data written by this version of the library.  Each structure
    change to the non_indexed_data should cause this version int to be
    incremented.
    
    @type internal_id: StringProperty
    @ivar internal_id: ID used internally by the system to identify this site.
    
    This should never be an auto incrementing field to reduce pain during
    migrations.
    
    This field should be indexed.
    
    @type public_id: StringProperty
    @ivar public_id: ID used when interacting with the site through a publicly
    accessible api.
    
    This field should be indexed.
    
    @type default: BooleanProperty
    @ivar default: When this site is in an iterable of sites to be searched,
    use this as default if a match cannot be found.
    
    This field should be indexed.
    
    @type created: DateTimeProperty
    @ivar created: Datetime when the model was created in the database.
    
    This field should be indexed.
    
    @type updated: DateTimeProperty
    @ivar updated: Datetime the model was last updated in the database.
    
    This field should be indexed.
    
    @type disabled: BooleanProperty
    @ivar disabled: Whether this site should not be used when matching.
    
    This field should be indexed.
    
    @type deleted: BooleanProperty
    @ivar deleted: Whether this site has been marked to never be used again.
    
    This field should be indexed.
    
    @type non_indexed_data: TextProperty
    @ivar non_indexed_data: JSON of data that should never be indexed.  It is
    stored in json so we can add to it in the future and have a version
    associated with the data.  When the application handles the schema changes,
    it makes it easier to migrate.
    
    @type non_indexed_version: IntegerProperty
    @ivar non_indexed_version: Integer representing the data structure version
    of the non_indexed_data.
    """
    
    
    CURRENT_NON_INDEXED_DATA_VERSION = 1
    
    
    internal_id = db.StringProperty(required=True, indexed=True)
    public_id = db.StringProperty(required=True, indexed=True)
    default = db.BooleanProperty(required=True, indexed=True, default=False)
    created = db.DateTimeProperty(required=True, indexed=True,auto_now_add=True)
    updated = db.DateTimeProperty(required=True, indexed=True, auto_now=True)
    disabled = db.BooleanProperty(required=True, indexed=True, default=False)
    deleted = db.BooleanProperty(required=True, indexed=True, default=False)
    non_indexed_data = db.TextProperty(required=True)
    
    non_indexed_version = db.IntegerProperty(required=True, indexed=False,
                                     default=CURRENT_NON_INDEXED_DATA_VERSION)