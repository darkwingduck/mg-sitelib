"""
Defines the library functions that are used to validate ids.
"""


from __future__ import unicode_literals
from sitelib.db.lib.getter import get_by_internal_id, get_by_public_id
import logging

log = logging.getLogger(__name__)


def DEFAULT_VALIDATE_INTERNAL_ID(connection, internal_id):
    """
    @summary: Validates a internal id to make sure it is unique.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type internal_id: Unicode
    @param internal_id: Internal id being checked for uniqueness
    
    @rtype: Boolean
    @return: Whether the internal id was unique or not. 
    """
    
    return get_by_internal_id(connection=connection,
                              internal_id=internal_id,
                              deleted=None,
                              disabled=None) is None


def DEFAULT_VALIDATE_PUBLIC_ID(connection, public_id):
    """
    @summary: Validates a public id to make sure it is unique.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type public_id: Unicode
    @param public_id: Public id being checked for uniqueness
    
    @rtype: Boolean
    @return: Whether the public id was unique or not. 
    """
    
    return get_by_public_id(connection=connection,
                            public_id=public_id,
                            deleted=None,
                            disabled=None) is None