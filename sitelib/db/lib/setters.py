"""
Defines the library functions that are used to set information
"""


from __future__ import unicode_literals
from sitelib.db.lib.generate import (DEFAULT_GENERATE_INTERNAL_ID,
     DEFAULT_GENERATE_PUBLIC_ID)

from sitelib.db.lib.validate import (DEFAULT_VALIDATE_INTERNAL_ID,
     DEFAULT_VALIDATE_PUBLIC_ID)

from sitelib.db.lib.helpers import determine_db
from sitelib.db.lib.getter import get_all
import logging

log = logging.getLogger(__name__)


def update(connection, site,
           generate_internal_id=DEFAULT_GENERATE_INTERNAL_ID,
           validate_internal_id=DEFAULT_VALIDATE_INTERNAL_ID,
           generate_public_id=DEFAULT_GENERATE_PUBLIC_ID,
           validate_public_id=DEFAULT_VALIDATE_PUBLIC_ID,
           _fail_first_generated_ids=False):
    
    """
    @summary: Updates a Site instance to the connected database.  The instance
    must exists before updating.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type site: sitelib.db.models.Site
    @param site: Site instance being saved to the database.  If the site
    does not have a _key, it will be created.
    
    If the site was created, it's internal_id and public_id will be created
    if they were not previously specified.
    
    @type generate_internal_id: Function
    @param generate_internal_id: Function used to generate a unique internal
    id for created sites.  This function takes no arguments and should return
    a unicode of 500 characters or less.
    
    This function will only be called if the internal_id has not been set yet.
    
    @type validate_internal_id: Function
    @param validate_internal_id: Function used to validate that a generated
    internal id is unique.  This function should take a connection and unicode
    and return True if the id is unique and False if it is not unique.
    
    This function will only be called if the internal_id has not been set yet.
    
    @type generate_public_id: Function
    @param generate_public_id: Function used to generate a unique public
    id for created sites.  This function takes no arguments and should return
    a unicode of 500 characters or less.
    
    This function will only be called if the public_id has not been set yet.
    
    @type validate_public_id: Function
    @param validate_public_id: Function used to validate that a generated
    public id is unique.  This function should take a connection and unicode
    and return True if the id is unique and False if it is not unique.
    
    This function will only be called if the public_id has not been set yet.
    
    @type _fail_first_generated_ids: Boolean
    @param _fail_first_generated_ids: Whether to fail the first generated
    internal and public ids.  This is only used by unittests.
    
    @rtype: Boolean
    @return: Whether the instance could be saved or not.
    """
    
    impl = determine_db(connection=connection)
    
    if impl:
        return impl.update(connection=connection,
                           site=site,
                           generate_internal_id=generate_internal_id,
                           validate_internal_id=validate_internal_id,
                           generate_public_id=generate_public_id,
                           validate_public_id=validate_public_id,
                           _fail_first_generated_ids=_fail_first_generated_ids)
    
    return False


def default(connection, site,
            generate_internal_id=DEFAULT_GENERATE_INTERNAL_ID,
            validate_internal_id=DEFAULT_VALIDATE_INTERNAL_ID,
            generate_public_id=DEFAULT_GENERATE_PUBLIC_ID,
            validate_public_id=DEFAULT_VALIDATE_PUBLIC_ID,
            _fail_implementation=False,
            _raise_implementation_exception=False,
            _fail_non_impl_defalt=False):
    
    """
    @summary: Sets the given site as the default.
    
    The site will only be set as default if it is not disabled and not
    deleted.
    
    If an error occurs and the underlying implentation does not support
    transactions, data corruption may occur.
    
    This method is optional to implementations.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type site: sitelib.db.models.Site
    @param site: Site instance being set as default.
    
    @type generate_internal_id: Function
    @param generate_internal_id: Function used to generate a unique internal
    id for created sites.  This function takes no arguments and should return
    a unicode of 500 characters or less.
    
    This function will only be called if the internal_id has not been set yet.
    
    @type validate_internal_id: Function
    @param validate_internal_id: Function used to validate that a generated
    internal id is unique.  This function should take a connection and unicode
    and return True if the id is unique and False if it is not unique.
    
    This function will only be called if the internal_id has not been set yet.
    
    @type generate_public_id: Function
    @param generate_public_id: Function used to generate a unique public
    id for created sites.  This function takes no arguments and should return
    a unicode of 500 characters or less.
    
    This function will only be called if the public_id has not been set yet.
    
    @type validate_public_id: Function
    @param validate_public_id: Function used to validate that a generated
    public id is unique.  This function should take a connection and unicode
    and return True if the id is unique and False if it is not unique.
    
    This function will only be called if the public_id has not been set yet.
    
    @type _fail_implementation: Boolean
    @param _fail_implementation: Don't use the database implementation if it
    available.  This is only used in unittesting.
    
    @type _raise_implementation_exception: Boolean
    @param _raise_implementation_exception: Whether to raise an exception to
    check the implementation failure.
    
    @type _fail_non_impl_defalt: Boolean
    @param _fail_non_impl_defalt: Whether to fail setting defaults to False
    for existing sites when the db doesn't implement its own method.  This is
    only used in unittesting.
    
    @rtype: Boolean
    @return: Whether the site could be set as default or not.
    """
    
    if not site.deleted and not site.disabled:
        if not _fail_implementation:
            impl = determine_db(connection=connection)
        
        else:
            impl = None
        
        #checking if the database implementation has a method to perform
        #this update in a single transaction
        if impl and hasattr(impl, "default"):
            try:
                if _raise_implementation_exception:
                    raise Exception("Failing implementation for unittest")
                
                return impl.default(connection=connection,
                                    site=site,
                                    generate_internal_id=generate_internal_id,
                                    validate_internal_id=validate_internal_id,
                                    generate_public_id=generate_public_id,
                                    validate_public_id=validate_public_id)
            
            except Exception as e:
                log.error("Could not set site as default for error:'%s'" % e)
                
                return False
        
        else:
            all_sites = get_all(connection=connection,
                                deleted=None,
                                disabled=None)
            
            all_sites = filter(lambda x:x != site, all_sites)
            
            for s in all_sites:
                s.default = False
                valid = update(connection=connection,
                               site=s,
                               generate_internal_id=generate_internal_id,
                               validate_internal_id=validate_internal_id,
                               generate_public_id=generate_public_id,
                               validate_public_id=validate_public_id)
                
                if _fail_non_impl_defalt or not valid:
                    return False
            
            site.default = True
            return update(connection=connection, site=site,
                          generate_internal_id=generate_internal_id,
                          validate_internal_id=validate_internal_id,
                          generate_public_id=generate_public_id,
                          validate_public_id=validate_public_id)
    
    else:
        return False


def _set_property_and_new_default(connection, prop, value, site,
            new_default=None,
            generate_internal_id=DEFAULT_GENERATE_INTERNAL_ID,
            validate_internal_id=DEFAULT_VALIDATE_INTERNAL_ID,
            generate_public_id=DEFAULT_GENERATE_PUBLIC_ID,
            validate_public_id=DEFAULT_VALIDATE_PUBLIC_ID,
            _fail_default=False):
    
    """
    @summary: Sets a property on the given site to the given value.
    
    If the site is default, checks that the provided new_default's  is not
    disabled and not deleted.

    This method is not required by any implementation.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type prop: Unicode
    @param prop: Name of the property being set.
    
    @type value: Object
    @param value: Value of the property being set.
    
    @type site: sitelib.db.models.Site
    @param site: Site instance the property is being set on.
    
    @type new_default: Site
    @param new_default: Site to be set as default if the site with the
    property being set is default.
    
    @type generate_internal_id: Function
    @param generate_internal_id: Function used to generate a unique internal
    id for created sites.  This function takes no arguments and should return
    a unicode of 500 characters or less.
    
    This function will only be called if the internal_id has not been set yet.
    
    @type validate_internal_id: Function
    @param validate_internal_id: Function used to validate that a generated
    internal id is unique.  This function should take a connection and unicode
    and return True if the id is unique and False if it is not unique.
    
    This function will only be called if the internal_id has not been set yet.
    
    @type generate_public_id: Function
    @param generate_public_id: Function used to generate a unique public
    id for created sites.  This function takes no arguments and should return
    a unicode of 500 characters or less.
    
    This function will only be called if the public_id has not been set yet.
    
    @type validate_public_id: Function
    @param validate_public_id: Function used to validate that a generated
    public id is unique.  This function should take a connection and unicode
    and return True if the id is unique and False if it is not unique.
    
    This function will only be called if the public_id has not been set yet.
    
    @type _fail_default: Boolean
    @param _fail_default: Whether to fail setting the default.  This is used
    for unittesting.
    
    @rtype: Boolean
    @return: Whether the property could be set or not.
    """
    
    #these will be passed into methods that call update, I just hate seeing
    #all of this being passed over and over again.  The passing is neccessary
    #but there is no reason to cluter the code.
    update_optional_args = {
        "generate_internal_id" : generate_internal_id,
        "validate_internal_id" : validate_internal_id,
        "generate_public_id" : generate_public_id,
        "validate_public_id" : validate_public_id
    }
    
    #this will hold the original value of the property in case we need to
    #reset it due to a failed operation.
    original_value = getattr(site, prop, None)
    
    if site.default:
        if not new_default or new_default.disabled or new_default.deleted:
            return False
        
        else:
            site.default = False
            setattr(site, prop, value)
            
            if update(connection=connection,
                      site=site,
                      **update_optional_args):
                
                if (not _fail_default
                    and default(connection=connection,
                     site=new_default,
                     **update_optional_args)):
                
                    return True
                
                else:
                    #we couldn't set the new_default as default so revert the
                    #changes on this site
                    site.default = True
                    setattr(site, prop, original_value)
                    
                    update(connection=connection,
                           site=site,
                           **update_optional_args)
            
            return False
    
    else:
        site.disabled = True
        
        return update(connection=connection,
                      site=site,
                      **update_optional_args)


def disable(connection, site, new_default=None,
            generate_internal_id=DEFAULT_GENERATE_INTERNAL_ID,
            validate_internal_id=DEFAULT_VALIDATE_INTERNAL_ID,
            generate_public_id=DEFAULT_GENERATE_PUBLIC_ID,
            validate_public_id=DEFAULT_VALIDATE_PUBLIC_ID,
            _fail_default=False):
    
    """
    @summary: Disables a site.
    
    If the site is default, checks that the provided new_default's is not
    disabled and not deleted.

    This method is not required by any implementation.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type site: sitelib.db.models.Site
    @param site: Site instance being disabled.
    
    @type new_default: Site
    @param new_default: Site to be set as default if the site being disabled
    is the current default.
    
    @type generate_internal_id: Function
    @param generate_internal_id: Function used to generate a unique internal
    id for created sites.  This function takes no arguments and should return
    a unicode of 500 characters or less.
    
    This function will only be called if the internal_id has not been set yet.
    
    @type validate_internal_id: Function
    @param validate_internal_id: Function used to validate that a generated
    internal id is unique.  This function should take a connection and unicode
    and return True if the id is unique and False if it is not unique.
    
    This function will only be called if the internal_id has not been set yet.
    
    @type generate_public_id: Function
    @param generate_public_id: Function used to generate a unique public
    id for created sites.  This function takes no arguments and should return
    a unicode of 500 characters or less.
    
    This function will only be called if the public_id has not been set yet.
    
    @type validate_public_id: Function
    @param validate_public_id: Function used to validate that a generated
    public id is unique.  This function should take a connection and unicode
    and return True if the id is unique and False if it is not unique.
    
    This function will only be called if the public_id has not been set yet.
    
    @type _fail_default: Boolean
    @param _fail_default: Whether to fail setting the default.  This is used
    for unittesting.
    
    @rtype: Boolean
    @return: Whether the site could be disabled or not.
    """
    
    return _set_property_and_new_default(connection=connection,
                             prop="disabled",
                             value=True,
                             site=site,
                             new_default=new_default,
                             generate_internal_id=generate_internal_id,
                             validate_internal_id=validate_internal_id,
                             generate_public_id=generate_public_id,
                             validate_public_id=validate_public_id,
                             _fail_default=_fail_default)


def delete(connection, site, new_default=None,
           generate_internal_id=DEFAULT_GENERATE_INTERNAL_ID,
           validate_internal_id=DEFAULT_VALIDATE_INTERNAL_ID,
           generate_public_id=DEFAULT_GENERATE_PUBLIC_ID,
           validate_public_id=DEFAULT_VALIDATE_PUBLIC_ID,
           _fail_default=False):
    
    """
    @summary: Deletes a site.
    
    If the site is default, checks that the provided new_default's is not
    disabled and not deleted.
    
    This method is not required by any implementation.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type site: sitelib.db.models.Site
    @param site: Site instance being deleted.
    
    @type new_default: Site
    @param new_default: Site to be set as default if the site being disabled
    is the current default.
    
    @type generate_internal_id: Function
    @param generate_internal_id: Function used to generate a unique internal
    id for created sites.  This function takes no arguments and should return
    a unicode of 500 characters or less.
    
    This function will only be called if the internal_id has not been set yet.
    
    @type validate_internal_id: Function
    @param validate_internal_id: Function used to validate that a generated
    internal id is unique.  This function should take a connection and unicode
    and return True if the id is unique and False if it is not unique.
    
    This function will only be called if the internal_id has not been set yet.
    
    @type generate_public_id: Function
    @param generate_public_id: Function used to generate a unique public
    id for created sites.  This function takes no arguments and should return
    a unicode of 500 characters or less.
    
    This function will only be called if the public_id has not been set yet.
    
    @type validate_public_id: Function
    @param validate_public_id: Function used to validate that a generated
    public id is unique.  This function should take a connection and unicode
    and return True if the id is unique and False if it is not unique.
    
    This function will only be called if the public_id has not been set yet.
    
    @type _fail_default: Boolean
    @param _fail_default: Whether to fail setting the default.  This is used
    for unittesting.
    
    @rtype: Boolean
    @return: Whether the site could be deleted or not.
    """
    
    return _set_property_and_new_default(connection=connection,
                             prop="deleted",
                             value=True,
                             site=site,
                             new_default=new_default,
                             generate_internal_id=generate_internal_id,
                             validate_internal_id=validate_internal_id,
                             generate_public_id=generate_public_id,
                             validate_public_id=validate_public_id,
                             _fail_default=_fail_default)