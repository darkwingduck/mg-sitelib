"""
Defines the library functions that will most likely be abstracted by each
database implementation.

Note: When unittesting, you can only achieve 97% with google app engine.
"""


from __future__ import unicode_literals
from sitelib.db.lib.helpers import determine_db
import logging

log = logging.getLogger(__name__)


def get_by_internal_id(connection, internal_id, deleted=None, disabled=None):
    """
    @summary: Gets a Site from the database with the specified internal_id.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type internal_id: Unicode
    @param internal_id: Internal id of the site to be retrieved.
    
    @type deleted: Boolean
    @param deleted: Whether the Site has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Site is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @rtype: sitelib.db.models.Site
    @return: Loaded Site model or None of not found.
    """
    
    impl = determine_db(connection=connection)
    
    if impl:
        return impl.get_by_internal_id(connection=connection,
                                       internal_id=internal_id,
                                       deleted=deleted,
                                       disabled=disabled)
    
    return None


def get_by_internal_ids(connection, internal_ids, deleted=None, disabled=None):
    """
    @summary: Gets Sites from the database with the specified internal_id.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type internal_ids: Iterable
    @param internal_ids: Iterable of internal ids of the site to be retrieved.
    
    @type deleted: Boolean
    @param deleted: Whether the Site has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Site is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @rtype: Dict
    @return: Dictionary mapping of Sites with the internal id as key.  If no
    site could be loaded, it's value will be None.
    """
    
    impl = determine_db(connection=connection)
    
    if impl:
        return impl.get_by_internal_ids(connection=connection,
                                        internal_ids=internal_ids,
                                        deleted=deleted,
                                        disabled=disabled)
    
    return dict.fromkeys(internal_ids)


def get_by_public_id(connection, public_id, deleted=None, disabled=None):
    """
    @summary: Gets a Site from the database with the specified public_id.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type public_id: Unicode
    @param public_id: Public id of the site to be retrieved.
    
    @type deleted: Boolean
    @param deleted: Whether the Site has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Site is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @rtype: sitelib.db.models.Site
    @return: Loaded Site model or None of not found.
    """
    
    impl = determine_db(connection=connection)
    
    if impl:
        return impl.get_by_public_id(connection=connection,
                                     public_id=public_id,
                                     deleted=deleted,
                                     disabled=disabled)
    
    return None


def get_by_public_ids(connection, public_ids, deleted=None, disabled=None):
    """
    @summary: Gets Sites from the database with the specified public_id.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type public_ids: Iterable
    @param public_ids: Iterable of public ids of the site to be retrieved.
    
    @type deleted: Boolean
    @param deleted: Whether the Site has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Site is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @rtype: Dict
    @return: Dictionary mapping of Sites with the public id as key.  If no
    site could be loaded, it's value will be None.
    """
    
    impl = determine_db(connection=connection)
    
    if impl:
        return impl.get_by_public_ids(connection=connection,
                                      public_ids=public_ids,
                                      deleted=deleted,
                                      disabled=disabled)
    
    return dict.fromkeys(public_ids)


def get_default(connection, deleted=None, disabled=None):
    """
    @summary: Gets the default Site from the database.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type deleted: Boolean
    @param deleted: Whether the Site has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Site is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @rtype: sitelib.db.models.Site
    @return: Loaded Site model or None of not found.
    """
    
    impl = determine_db(connection=connection)
    
    if impl:
        return impl.get_default(connection=connection,
                                deleted=deleted,
                                disabled=disabled)
    
    return None


def get_domains_for_site(connection, site, deleted=None, disabled=None,
                         _force_import_error=False):
    
    """
    @summary: Gets the domains for the given site.
    
    This method required that domainlib is installed.  If it is not installed,
    it will fail gracefully and return and empty set of domains.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type site: Site
    @param site: Site instance who's internal id will be used to retrieve
    domains.
    
    @type deleted: Boolean
    @param deleted: Whether the domain has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the domain is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @type _force_import_error: Boolean
    @param _force_import_error: Forces the import of domainlib not to work. Used
    for unittesting.
    
    @rtype: Set
    @return: Set of domains that the given site is owner.
    """
    
    dblib = None

    try:
        from domainlib.db import lib as dblib
        
        if _force_import_error:
            dblib = None
            raise Exception("Forced import error")
        
    except Exception as e:
        log.error("Could not load domainlib for error:%s" % e)
    
    if dblib:
        return dblib.get_by_owner_id(connection=connection,
                                    owner_id=site.internal_id,
                                    deleted=deleted,
                                    disabled=disabled)
    
    else:
        return set()


def get_default_domain_for_site(connection, site, deleted=None, disabled=None,
                                _force_import_error=False):
    
    """
    @summary: Gets the domains for the given site.
    
    This method required that domainlib is installed.  If it is not installed,
    it will fail gracefully and return None.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type site: Site
    @param site: Site instance who's internal id will be used to retrieve
    domains.
    
    @type deleted: Boolean
    @param deleted: Whether the domain has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the domain is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @type _force_import_error: Boolean
    @param _force_import_error: Forces the import of domainlib not to work. Used
    for unittesting.
    
    @rtype: domainlib.db.models.Domain
    @return: Default domain for the site or None.
    """
    
    dblib = None

    try:
        from domainlib.db import lib as dblib
        
        if _force_import_error:
            dblib = None
            raise Exception("Forced import error")
        
    except Exception as e:
        log.error("Could not load domainlib for error:%s" % e)
    
    if dblib:
        return dblib.get_default_by_owner_id(connection=connection,
                                            owner_id=site.internal_id,
                                            deleted=deleted,
                                            disabled=disabled)
    
    else:
        return None


def get_site_using_host(connection, host, site_deleted=None, site_disabled=None,
                        domain_deleted=None, domain_disabled=None,
                        _force_import_error=False):
    
    
    """
    @summary: Loads a site instance by determining which domain the host
    resovles to.
    
    This method required that domainlib is installed.  If it is not installed,
    it will fail gracefully and return None.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type host: Unicode
    @param host: Unicode that contains a host name or some other uri that has
    a scheme, sub domain, and root zone.
    
    @type site_deleted: Boolean
    @param site_deleted: Whether the site has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type site_disabled: Boolean
    @param site_disabled: Whether the site is currently disabled.  Pass None if
    you don't care if it is disabled or not.
    
    @type domain_deleted: Boolean
    @param domain_deleted: Whether the domain has been marked as deleted.  Pass
    None if you don't care if it is deleted or not.
    
    @type domain_disabled: Boolean
    @param domain_disabled: Whether the domain is currently disabled.  Pass None
    if you don't care if it is disabled or not.
    
    @type _force_import_error: Boolean
    @param _force_import_error: Forces the import of domainlib not to work. Used
    for unittesting.
    
    @rtype: Site
    @return: Site instance which has a domain that resolves the host, the
    default site if one is specified, or None if no site is specified.
    """
    
    dblib = None

    try:
        from domainlib.db import lib as dblib
        
        if _force_import_error:
            dblib = None
            raise Exception("Forced import error")
                 
    except Exception as e:
        log.error("Could not load domainlib for error:%s" % e)
    
    if dblib:
        domain = dblib.get_resolves_host(connection=connection,
                                         host=host,
                                         deleted=domain_deleted,
                                         disabled=domain_disabled)

        site = None
        
        if domain:
            site = get_by_internal_id(connection=connection,
                                      internal_id=domain.owner_id,
                                      deleted=site_deleted,
                                      disabled=site_disabled)
        
        if not site:
            site = get_default(connection=connection,
                               deleted=site_deleted,
                               disabled=site_disabled)
            
        return site
    
    else:
        return None


def get_all(connection, deleted=None, disabled=None):
    """
    @summary: Gets all the sites.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type deleted: Boolean
    @param deleted: Whether the Site has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Site is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @rtype: Set
    @return: Set containing all loaded sites.
    """
    
    impl = determine_db(connection=connection)
    
    if impl:
        return impl.get_all(connection=connection,
                            deleted=deleted,
                            disabled=disabled)
    
    return set()