"""
Defines the library functions that do not require the database for use.
"""


from __future__ import unicode_literals
import logging

log = logging.getLogger(__name__)


def get_by_internal_id_from_iterable(sites, internal_id, deleted=None,
                                     disabled=None):
    """
    @summary: Gets a site in a iterable that has a matching internal_id.
    
    @type sites: Iterable
    @param sites: Iterable containing sitelib.db.models.Site instances.
    
    @type internal_id: Unicode
    @param internal_id: Internal id of the site to be retrieved.
    
    @type deleted: Boolean
    @param deleted: Whether the Site has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Site is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @rtype: sitelib.db.models.Site or None
    @return: Site instance with matching internal_id or None if no site
    could be found.
    """
    
    if deleted is not None:
        sites = filter(lambda x:True if x.deleted == deleted else False,
                       sites)
    
    if disabled is not None:
        sites = filter(lambda x:True if x.disabled == disabled else False,
                       sites)
    
    for site in sites:
        if site.internal_id == internal_id:
            return site
    
    return None


def get_by_internal_ids_from_iterable(sites, internal_ids, deleted=None,
                                      disabled=None):
    """
    @summary: Gets all sites in a iterable that have an internal_id in
    internal_ids.
    
    @type sites: Iterable
    @param sites: Iterable containing sitelib.db.models.Site instances.
    
    @type internal_ids: Iterable
    @param internal_ids: Iterable of internal ids of the sites to be
    retrieved.
    
    @type deleted: Boolean
    @param deleted: Whether the Site has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Site is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @rtype: Dict
    @return: Dictionary mapping of sites with internal_id being key and
    site being value.  If no site can be found for a internal_id, it's
    value will be None.
    """
    
    found = dict.fromkeys(internal_ids)
    
    if deleted is not None:
        sites = filter(lambda x:True if x.deleted == deleted else False,
                       sites)
    
    if disabled is not None:
        sites = filter(lambda x:True if x.disabled == disabled else False,
                       sites)
    
    for site in sites:
        if site.internal_id in internal_ids:
            found[site.internal_id] = site
    
    return found


def get_by_public_id_from_iterable(sites, public_id, deleted=None,
                                   disabled=None):
    """
    @summary: Gets a site in a iterable that has a matching public_id.
    
    @type sites: Iterable
    @param sites: Iterable containing sitelib.db.models.Site instances.
    
    @type public_id: Unicode
    @param public_id: Public id of the site to be retrieved.
    
    @type deleted: Boolean
    @param deleted: Whether the Site has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Site is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @rtype: sitelib.db.models.Site or None
    @return: Site instance with matching public_id or None if no site
    could be found.
    """
    
    if deleted is not None:
        sites = filter(lambda x:True if x.deleted == deleted else False,
                       sites)
    
    if disabled is not None:
        sites = filter(lambda x:True if x.disabled == disabled else False,
                       sites)
    
    for site in sites:
        if site.public_id == public_id:
            return site
    
    return None


def get_by_public_ids_from_iterable(sites, public_ids, deleted=None,
                                    disabled=None):
    """
    @summary: Gets all sites in a iterable that have an public_id in
    public_ids.
    
    @type sites: Iterable
    @param sites: Iterable containing sitelib.db.models.Site instances.
    
    @type public_ids: Iterable
    @param public_ids: Iterable of public ids of the sites to be
    retrieved.
    
    @type deleted: Boolean
    @param deleted: Whether the Site has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Site is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @rtype: Dict
    @return: Dictionary mapping of sites with public_id being key and
    site being value.  If no site can be found for a public_id, it's
    value will be None.
    """
    
    found = dict.fromkeys(public_ids)
    
    if deleted is not None:
        sites = filter(lambda x:True if x.deleted == deleted else False,
                       sites)
    
    if disabled is not None:
        sites = filter(lambda x:True if x.disabled == disabled else False,
                       sites)
    
    for site in sites:
        if site.public_id in public_ids:
            found[site.public_id] = site
    
    return found


def get_default_from_iterable(sites, deleted=None, disabled=None):
    """
    @summary: Gets a site in a iterable that is marked as default
    
    @type sites: Iterable
    @param sites: Iterable containing sitelib.db.models.Site instances.
    
    @type deleted: Boolean
    @param deleted: Whether the Site has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Site is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @rtype: sitelib.db.models.Site or None
    @return: Site instance marked default or None if no site could be found.
    """
    
    if deleted is not None:
        sites = filter(lambda x:True if x.deleted == deleted else False,
                       sites)
    
    if disabled is not None:
        sites = filter(lambda x:True if x.disabled == disabled else False,
                       sites)
    
    for site in sites:
        if site.default:
            return site
    
    return None