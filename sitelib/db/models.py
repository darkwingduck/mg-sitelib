"""
Defines the data model for site instances.
"""


from __future__ import unicode_literals


class Site(object):
    """
    @summary: Defines the data model for a site instance.
    
    @type _key: Object
    @ivar _key: Database specific object or unique identifier for the site.
    
    @type internal_id: Unicode
    @ivar internal_id: ID used to identify and relate the site internally.
    
    Needs to be indexed.
    
    @type public_id: Unicode
    @ivar public_id: ID used to identify the site when accessed publicly.
    
    Needs to be indexed.
    
    @type default: Boolean
    @ivar default: If no domains can be matched to a site, use this site as
    default.
    
    Needs to be indexed.
    
    @type name: Unicode
    @ivar name: Name displayed with information about the site.
    
    @type description: Unicode
    @ivar description: Description displayed with information about the site.
    
    @type created: DateTime
    @ivar created: DateTime when the site was created.
    
    Needs to be indexed.
    
    @type updated: DateTime
    @ivar updated: DateTime when the site information was last updated.
    
    Needs to be indexed.

    @type disabled: Boolean
    @ivar disabled: Whether this site should not be used when matching.
    
    This field should be indexed.
    
    @type deleted: Boolean
    @ivar deleted: Whether this site has been marked to never be used again.
    
    This field should be indexed.
    """
    
    
    def __init__(self):
        self._key = None
        self.internal_id = ""
        self.public_id = ""
        self.name = ""
        self.description = ""
        self.created = None
        self.updated = None
        self.default = False
        self.disabled = False
        self.deleted = False
    
    
    def __eq__(self, other):
        """
        @summary: Checks to see if the two site's internal id match.
        """
        
        return other.internal_id == self.internal_id
    
    
    def __ne__(self, other):
        """
        @summary: Checks to see if the two site's internal id do not match.
        """
        
        return other.internal_id != self.internal_id