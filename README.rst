Summary
=======
Library for determining a site instance based on a domain or public id.

Roadmap
=======
* 0.4.0: Current

 - Create unit test for default, disable, and enable db methods
 
 - Release to pypi instance
 
* 0.3.1:
 
 - Split up db lib into package
 
 - Split out lib unittest so all the test don't need to be duplicated when 
   implementing new databases

 - Add method to set a specific enabled/not deleted site as the default and
   turns default off for all other sites including disabled and deleted

* 0.3.0:

 - Create unit test for domain integration
 
 - Release to pypi instance

* 0.2.1: 

 - Add methods to DAL to relate Domains from DomainLib to sites
 
 	1. Get domains for a specific site
 	
 	2. Determine site instance by using root zones and site domains

* 0.1.0:
 
 - Create unit test for site dal
 
 - Release to pypi instance

* 0.0.3: 

 - Create DAL for google app engine

* 0.0.2:

 - Create a database site model spec with identifying information for relating
   sites
   
 - Create method to get a site from a iterable of sites by its internal id
 
 - Create method to get a site from a iterable of sites by its public id
 
 - Define a database abstraction layer (DAL) which can
 
 	1. Perform CRUD operations
 	
 	3. Get a site by its internal id
 	
 	4. Get a site by its public id

* 0.0.1:

 - Create setup.py for use with easy_install and pip
